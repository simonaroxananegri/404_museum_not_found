@extends('layouts.app')

@section('content')





<div class="container pt-5">
    <div class="row text-center my-5 justify-content-center" >
        
        <div class="col-12 col-md-8">
            <div class="card border-0 shadow mb-5 bg-body">
                <div class="bg-frame-show text-center">
                    <h3 class="card-title text-my-w py-5">{{ $article->title }}</h3>
                </div>
                <div class="card-body">
                    <p class="card-text text-left">Categoria: 
                        <a href="{{ route('categories.index', $article->category) }}">{{ $article->category->name }}</a></p>
                        @foreach($article->tags as $tag)
                        <small class="d-inline-block">#{{ $tag->name }}</small>
                        @endforeach
                        <p class="card-text text-left"><strong>Problema: </strong> {{ $article->body }}</p>
                        @if ($article->img==null)
                        <div id="senzaimg" class="border-zoom  d-none">
                            <a   href="{{Storage::url($article->img) }}"><img src="{{Storage::url($article->img) }}" class="img-fluid" alt="Screen del codice" ></a>
                        </div>     
                        
                        @else
                        <div id="senzaimg" class="border-zoom">
                            <a   href="{{Storage::url($article->img) }}"><img src="{{Storage::url($article->img) }}" class="img-fluid" alt="Screen del codice" ></a>
                        </div>     
                        
                        @endif
                        
                        <small class="d-block text-left">Scritto da: {{ $article->user->name }}</small>
                        <small class="d-block text-left">Scritto il: {{ $article->created_at->format('d/m/y') }}</small>
                        <hr>
                        <p class="card-text text-left"> <strong> Soluzione: </strong> {{ $article->solution }}</p>
                      
                        @foreach ($article->comments as $comment)
                        <div class="col-12">
                            <p class="card-text text-left"> <strong>Soluzione alternativa: </strong>{{ $comment->comment }}</p>
                            <small  class="d-block text-left">{{ $comment->user->name }}</small>
                            <small  class="d-block text-left">{{ $comment->created_at->format('d/m/y')}}</small>
                        </div>
                        @if ($comment->user->id == Auth::id() || Auth::user()->isAdmin())
                        <form action="{{ route('comment.delete', $comment) }}" method="POST">
                            @csrf
                            @method('DELETE')
                            <button class="btn btn-danger">Elimina soluzione</button>
                            
                        </form>
                        @endif
                  
                        @endforeach
                        
                        <div>
                            @if(Auth::user() && Auth::user()->isAdmin())
                            <form class=" form-group" action="{{ route('comment.store', $article) }}" method="POST">
                                @csrf   
                                <div>
                                    <textarea name="comment" id="" cols="30" rows="3" class="form-control my-2"></textarea>
                                </div>
                                <a class="text-my-w" role="button">
                                    <button type="submit" class="btn button-custom rounded-pill">inserisci la tua soluzione</button>  
                                </a>
                            
                            </form>
                            
                         
                            <form action="{{ route('articles.delete', $article) }}" method="post">
                                @csrf
                                @method('DELETE')
                                <input type="text" name="message">
                                <button type="submit" class="btn btn-danger">Elimina</button>
                            </form>
                            
                            @endif
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
        
        
        
        
        
        
        @endsection