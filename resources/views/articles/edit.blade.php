@extends('layouts.app')

@section('content')
<div class="container pt-5">
    <div class="row text-center my-5">
        <div class="col-12">
            <h1>Modifica il tuo articolo</h1>
        </div>
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="col-12">
            <form method="POST" action="{{ route('articles.update', $article) }}" enctype="multipart/form-data">
                @csrf
                <div class="form-group">
                    <label>Titolo dell' articolo</label>
                    <input type="text" name="title" class="form-control" value="{{ $article->title}}">
                </div>
                <div class="form-group">
                    <label for="category_id">Categoria</label>
                    <select name="category_id"class="form-control">
                        @foreach ($categories as $category)
                        <option value="{{ $category->id }}" {{ $article->category->id==$category->id  ? 'selected':''}}>{{ $category->name }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label>Immagine dell' articolo</label>
                    <input src="{{ Storage::url($article->img) }}" type="file" name="img" class="form-control-file img-fluid w-25">
                </div>
                <div class=" form-group">
                    <label>Descrivi il tuo errore</label>
                    <textarea name="body" id="" cols="30" rows="10" class="form-control" value="">{{ $article->body}}</textarea>
                </div>
                <div class=" form-group">
                    <label> La soluzione (facoltativo)</label>
                    <textarea name="solution" id="" cols="30" rows="10" class="form-control form-shadow">{{ $article->solution}}</textarea>
                </div>
                
                <button type="submit" class="btn button-custom rounded-pill mx-2">Submit</button>
            </form>
        </div>
    </div>
</div>




    
@endsection