@extends('layouts.app')

@section('content')
<div class="container pt-5">
    <div class="row text-center my-5">
        <div class="col-12">
            <h1>Contattaci</h1>
        </div>
    </div>
</div>   
<div class="container ">
  <div class="row justify-content-center">
    <div class=" col-12 col-lg-6 formRicetta p-2 p-lg-5 ">
      <div class="text-center">
        <h3>Mandaci la tua richiesta</h3>
      </div>
      <form method="POST" action="{{ route('contact.send') }}">
        @csrf
          <div class="form-group mt-5 mb-3">
            <label for="name" class="form-label">Nome</label>
            <input name="name" type="text" class="form-control shadow mb-5 bg-body border-0 rounded-pill formFinestra" id="nome" value="{{old ('name')}}">
            @error('name')
            <div class="alert alert-danger">{{$message}}</div>   
            @enderror
          </div>
          <div class=" form-group mb-3">
            <label for="exampleInputEmail1" class="form-label">Email </label>
            <input name="email" type="email" class="form-control  shadow mb-5 bg-body  border-0 rounded-pill formFinestra" id="exampleInputEmail1" aria-describedby="emailHelp" value="{{old ('email')}}">
            @error('email')
            <div class="alert alert-danger">{{$message}}</div>   
            @enderror
            <div id="emailHelp" class="form-text">
              La tua mail non verrà mai mostrata ad altri utenti.
            </div>
          </div>
          <div class=" form-group mb-5 ">
            <label class="form-label" for="message">Scrivi qui il tuo problema</label>
            <textarea name="message" type="textarea" class="form-control  shadow mb-5 bg-body border-0 formFinestra" id="message" cols="40" rows="10">{{old ('message')}}</textarea>
            @error('message')
            <div class="alert alert-danger">{{$message}}</div>   
            @enderror
          </div>
          <button type="submit" class="btn button-custom rounded-pill mt-4 mx-auto">Invia</button>
      </form>
    </div>
  </div>
</div>


@endsection
