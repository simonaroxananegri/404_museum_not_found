

<nav class="navbar navbar-expand-md navbar-light fixed-top bg-nav ">
    <div class="container-fluid">
        <img src="/media/logo_404_colorato.png" alt="" class="img-fluid mr-3" width="50px">
        <a class="navbar-brand" href="{{ url('/') }}">
            {{ config('app.name', 'Laravel') }}
        </a>
        <button class="navbar-toggler border-0" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
            <span class="navbar-toggler-icon "></span>
        </button>
        
        
        
        
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            
            
            <a class="navbar-brand" href="{{ route('articles.index') }}">Articoli</a>
            
            <a class="navbar-brand" href="#error">Inserisci i tuoi errori</a>
            
            <ul class="navbar-nav">
                <div class="nav-item dropdown navbar-brand">
                    <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                        Categorie
                    </a>
                    @auth
                    <li class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                        @foreach ($categories as $category)
                        <a class="dropdown-item" href="{{ route('categories.index', $category) }}">{{ $category->name }}</a>
                        @endforeach
                    </li>
                    @endauth
                </div>
            </ul>
            
            <a class="navbar-brand" href="{{ route('contact.index')}}">Contattaci</a>
            
            
            <!-- Left Side Of Navbar -->
            <ul class="navbar-nav mr-auto">
                
            </ul>
            
            <!-- Right Side Of Navbar -->
            <ul class="navbar-nav ml-auto">
                <!-- Authentication Links -->
                @guest
                @if (Route::has('login'))
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                </li>
                @endif
                
                @if (Route::has('register'))
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                </li>
                @endif
                @else


                <li class="nav-item dropdown">
                    <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                        {{ Auth::user()->name }}
                    </a>

                  
 
                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="{{ route('logout') }}"
                        onclick="event.preventDefault();
                        document.getElementById('logout-form').submit();">
                        {{ __('Logout') }}
                        </a>
                    
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                        @csrf
                        </form>
                        <a class="dropdown-item" href="{{ route('users.profile') }}">
                        Profilo
                        </a>
                    </div>
                </li>
            
                @endguest
            </ul>
        
            <form action="{{ route('search.index') }}" method="GET" class="d-flex">
                <input class="form-control bg-white border-0 rounded-pill my-2 me-2" type="text"  placeholder="Ricerca..." name="searchWord" id="searchWord" required/>
                <button class="btn button-custom rounded-pill " type="submit">Ricerca</button>
            </form>

            @if ( Auth::user() && Auth::user()->isAdmin() )
                <a href=" {{ route('admin.dashboard') }} " class="nav-link">Admin page</a>
            @endif

        </div>
    </div>

</nav>








