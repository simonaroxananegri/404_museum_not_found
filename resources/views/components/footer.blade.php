<div class="container-fluid">
        <div class="row justify-content-center align-items-center h-100 footer ">
    
            <div class=" px-3  col-12 col-md-4 my-3 flex-column align-items-center justify-content-center">
                <div class="d-flex align-items-center">
                    <a href="{{route('home')}}"> <img src="/media/logo_404_bianco.png" class="img-fluid d-block logo" alt="logo-sito"> </a>
                </div>
                <p class="mt-2">Lorem ipsum, dolor sit amet consectetur adipisicing elit. </p>
                <ul class="contacts">
                    <li>Aiuto</li>
                    <li>Regole</li>
                    <li>Condizioni</li>
                    <li>Privacy</li>
                    <li>Chi siamo</li>
                    <li>Lavora con noi</li>
                    
                </ul>
                <a href="#" target="a_blank"><i class="fab fa-facebook fa-2x mx-1 "></i></a>
                <a href="#" target="a_blank"><i class="fab fa-instagram fa-2x mx-1"></i></a>
                <a href="#" target="a_blank"><i class="fab fa-linkedin fa-2x mx-1"></i></a>
    
            </div>
    
            <div class="px-3 col-12 col-md-8 my-3 justify-content-center">
            <div class="container">
    <div class="row">
        <div class="col-12" id="error">
            <form method="POST" action="{{ route('articles.store') }}" enctype="multipart/form-data">
                @csrf
                <div class="row">
                <div class="form-group col-lg-4">
                    <label>Titolo dell' articolo</label>
                    <input type="text" name="title" class="form-control rounded-pill form-shadow">
                    @error('title')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>
                <div class="form-group col-lg-4">
                    <label for="category_id">Categoria</label>
                    <select name="category_id" class="form-control rounded-pill form-shadow">
                        @foreach($categories as $category)
                        <option value="{{ $category->id }}">{{ $category->name }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group col-lg-4">
                    <label>Tag</label>
                    <select name="tag[]" class="form-control tag-radius form-shadow" multiple>
                        @foreach($tags as $tag) 
                        <option value="{{ $tag->id }}">{{ $tag->name }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group col-lg-4">
                    <label>Immagine (facoltativo) <br><small>(max 10MB)</small></label>
                    <input type="file" name="img" class="form-control-file form-shadow ">
                </div>
                </div>
                <div class=" form-group">
                <a class="text-my-w" data-toggle="collapse" href="#collapseErrore" role="button" aria-expanded="false" aria-controls="collapseErrore"><label>> L'errore</label></a>
                @error('body')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                <hr class="hr-my-white">
                <div class="collapse" id="collapseErrore">
                 <textarea name="body" id="" cols="30" rows="7" class=" my-3 form-control form-shadow"></textarea>
                 </div>
                </div>
                <div class=" form-group">
                <a class="text-my-w" data-toggle="collapse" href="#collapseSolution" role="button" aria-expanded="false" aria-controls="collapseSolution">
                    <label>> La soluzione (facoltativo)</label>
                </a>
                <hr class="hr-my-white">
                <div class="collapse" id="collapseSolution">
                    <textarea name="solution" id="" cols="30" rows="7" class="form-control my-3 form-shadow"></textarea>
                </div>
                </div>
                <button type="submit" class="btn button-custom rounded-pill">Invia</button>
            </form>
        </div>
    </div>
</div>



            </div>
    
        </div>
        <div class="row justify-content-center align-items-center footer-copyright">
    
            <p class="text-center pb-0">© 2021 404MuseumNotFound.it - P.IVA 0345433526440962 |</p>
            <p>Power by Valentina, Simona, Damiano </p>
    
    
        </div>
    
    </div>