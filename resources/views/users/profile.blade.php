@extends('layouts.app')

@section('content')

<div class="container pt-5">
    <div class="row text-center my-5">
        <div class="col-12">
        <h1>Profilo utente, {{ $user->name}}</h1>
        </div>
    </div>
</div>

<div class="container">
    <div class="row">
        <div class="col-12 col-md-4">
            @if (session('message'))
            <div class="alert alert-success">
                {{ session('message') }}
            </div>
            @endif
        </div>
    </div>
</div>

<div class="container my-5">
    <div class="row justify-content-center align-items-center">
        <div class="col-12 col-md-8">
            <table class="table border">
                <thead>
                    <tr>
                        <th scope="col">title</th>
                        <th scope="col">body</th>
                        <th scope="col">actions</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($user->articles as $article)
                    
                    <tr>
                        
                        <td>
                            <a href="{{ route('articles.show', $article) }}" class="btn "><strong>{{ $article->title }}</strong></a>
                        </td>
                        <td>{{ substr($article->body, 0, 20) }} ...</td>
                        <td>
                            <div class="row">
                                <div class="col-8">
                             <a href="{{ route('articles.edit', $article) }}" class="btn button-custom rounded-pill mx-2">Modifica articolo</a>
                             </div>
                             <div class="col-4">
                            <form action="{{ route('articles.destroy', $article) }}" method="POST">
                                @csrf
                                @method('DELETE')
                                <button  href="#" class="btn button-custom rounded-pill mx-2" alt="cancella" ><i class="fas fa-times"></i></button>
                            </form> 
                            </div>  
                            </div>
                            
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>



@endsection