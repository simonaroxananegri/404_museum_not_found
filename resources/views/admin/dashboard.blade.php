@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row mt-5 justify-content-center text-center">
        <div class="col-12">
            <h2>Bentornato: {{Auth::user()->name}} </h2>
        </div>
        <div class="col-12">
            
        </div>
    </div>
</div>

<div class="container">
    <div class="row my-5">
        <div class="col-12">
            <h3>Gestisci gli utenti</h3>
        </div>
        <div class="col-12">
            <table class="table table-dark border">
                <thead>
                    <tr>
                        <th scope="col">id</th>
                        <th scope="col">Name</th>
                        <th scope="col">email</th>
                        <th scope="col">iscritto il</th>
                        <th scope="col">n articoli</th>
                        <th scope="col">actions</th>
                        
                    </tr>
                    
                </thead>
                
                <tbody>
                   
                    @foreach ($users as $user)
                    <tr>
                        <th scope="row">{{ $user->id }}</th>
                        <td>{{ $user->name }}</td>
                        <td>{{ $user->email }}</td>
                        <td>{{ $user->created_at->format('d/m/Y') }}</td>
                        <td>
                            <a href="{{ route('articles.for.user', $user) }}">
                                {{ count($user->articles) }}
                            </a>
                        </td>
                        <td class="d-inline">
                            <form action="{{ route('user.delete', $user) }}" method="post">
                                @csrf
                                @method('DELETE')
                                <input type="text" name="message">
                                <button class="btn btn-danger">Elimina</button>
                            </form>
                            @if ($user->disable == false)
                            <a href="{{ route('toggle.user.disable', $user) }}" class="btn btn-warning">Disabilita</a>
                            @else
                            <a href="{{ route('toggle.user.disable', $user) }}" class="btn btn-primary">Abilita</a>
                            @endif
                        </td> 
                    </tr>
                    @endforeach
                    
                    
                </tbody>

            </table>
            
            
        </div>
    </div>
</div>
{{--  --}}

@endsection