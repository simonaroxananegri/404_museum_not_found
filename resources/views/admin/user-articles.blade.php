@extends('layouts.app')

@section('content')
 <div class="container">
     <div class="row mt-5">
         <div class="col-12">
             <h1>Tutti gli articoli di: {{$user->name}}</h1>
         </div>
     </div>
 </div>
 
 <div class="container">
    <div class="row my-5">
        @foreach ($articles as $article)

        <div class="col-12 col-md-6 col-lg-4  my-3">
            <div class="card card-scale border-0 shadow mb-5 bg-body h-100">
                <div class="row justify-content-center align-items-center">
                    <div class="col-12">  
                        <div class="bg-frame card-img-top text-center">
                        <h3 class="card-title bg-frame-title text-my-w">{{ $article->title }}</h3>
                        </div>
                    </div>
                </div>
                <div class="card-body d-flex flex-column justify-content-center">
                    <p class="card-text ">Categoria: 
                        <span class="ml-3"><a href="{{ route('categories.index', $article->category) }}">{{ $article->category->name }}</a></span></p>
                        @foreach($article->tags as $tag)
                            <small class="d-inline-block">#{{ $tag->name }}</small>
                        @endforeach
                        <p class="card-text text-left"><strong>Problema: </strong> {{substr($article->body , 0 , 180) }}...</p>
                        <small class="d-block">Scritto da: {{ $article->user->name }}</small>
                    <a href="{{ route('articles.show', $article) }}" class="btn button-custom rounded-pill w-50 mt-3">Vedi articolo</a>
                </div>
            </div>
            
        </div>


        @endforeach
    </div>
</div>


@endsection