@extends('layouts.app')

@section('content')

<div class="container-fluid bg-img">
    <div class="row justify-content-center text-center text-shadow align-items-center text-dark h-100">
        <div class="col-12 col-md-9">
            <img src="/media/logo_404_colorato.png" class="img-fluid w-25 logo-sito">
            <h1 class="text-center  title-blog my-5">404 | MuseumNotFound</h1>
            <h2 class="text-center subtitle-blog my-5">Il Museo degli Errori</h2>
            <a href="{{route('articles.index')}}" class="btn button-custom rounded-pill mx-2">Scopri gli articoli</a>
            <a href="#" class="btn button-custom rounded-pill mx-2">Contattaci</a>
        </div>
    </div>
</div>


<div class="container">
    <div class="row justify-content-center">
        <div class="col-12 my-5">
            <h1>Benvenuto, {{Auth::user()->name }}</h1>
            
        </div>
        @if (session('message'))
        <div class="alert alert-success">
            {{ session('message') }}
        </div>
        @endif
    </div>
</div>

<!--ULTIMI 6 ARTICOLI -->


<div class="container">
    <h3 class="small mt-3 h3">Articoli</h3>
    <h2>GLi ultimi 6 articoli</h2>
    <div class="row my-5">
       
    @foreach ($lastsix as $article)

    <div class="col-12 col-md-6 col-lg-4 my-3">
            <div class="card card-scale border-0 shadow mb-5 bg-body h-100">
                <div class="row justify-content-center align-items-center">
                    <div class="col-12">  
                        <div class="bg-frame card-img-top  text-center">
                        <h3 class="card-title bg-frame-title">{{ $article->title }}</h3>
                        </div>
                    </div>
                </div>
                <div class="card-body d-flex flex-column justify-content-center">
                    <p class="card-text ">Categoria: 
                        <span class="ml-3"><a href="{{ route('categories.index', $article->category) }}">{{ $article->category->name }}</a></span></p>
                       
                        @foreach($article->tags as $tag)
                            <small class="d-inline-block">#{{ $tag->name }}</small>
                        @endforeach
                       
                        <p class="card-text text-left"><strong>Problema: </strong> {{substr($article->body , 0 , 180) }}...</p>
                        <small class="d-block">Scritto da: {{ $article->user->name }}</small>
                    <a href="{{ route('articles.show', $article) }}" class="btn button-custom rounded-pill w-50 mt-3">Vedi articolo</a>
                </div>
            </div>
            
        </div>


    @endforeach
    
    </div>
    <div class="d-flex justify-content-center">
      {{--   {!! $articles->links() !!} --}}
    </div>
</div>



<div class="container">
    <h3 class="small mt-3 h3">Contatti</h3>
    <h2>Come contattarci</h2>
    <div class="row justify-content-between align-items-center my-5 mx-auto">
        <div class="col-12 col-lg-5 my-3 px-0 article">

            <img src="media/slider_home.png" class="img-fluid mx-0 d-block shadow" alt="elefante-con-umana">

        </div>

        <div class="col-12 col-lg-6 my-3 ">
            <h2>Hai bisogno di qualche informazione?</h2>
            <h3 class="small mt-3 h3">Descrizione</h3>
            <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Iure quod nemo explicabo eius
                suscipit
                quidem provident dolores soluta beatae totam fugit rerum adipisci, vel unde placeat
                exercitationem repellat consequuntur iusto?</p>
            <a href="{{ route('contact.index')}}" class="btn button-custom rounded-pill mx-2"> Contattaci </a>

        </div>

    </div>


</div>



@endsection

