<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Middleware\AdminMiddleware;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\SearchController;
use App\Http\Controllers\ArticleController;
use App\Http\Controllers\CommentController;
use App\Http\Controllers\ContactController;
use App\Http\Controllers\CategoryController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();

Route::get('/', [HomeController::class, 'home'])->name('home');
Route::get('/index', [HomeController::class, 'index'])->name('index');
Route::get('/user/profile', [HomeController::class, 'profile'])->name('users.profile');

Route::get('/articles/index', [ArticleController::class, 'index'])->name('articles.index');
Route::post('/articles/store', [ArticleController::class, 'store'])->name('articles.store');
Route::get('/articles/create', [ArticleController::class, 'create'])->name('articles.create');
Route::get('/articles/{article}/show', [ArticleController::class, 'show'])->name('articles.show');
Route::post('/articles/{article}/update', [ArticleController::class, 'update'])->name('articles.update');
Route::get('/articles/{article:title}/edit', [ArticleController::class, 'edit'])->name('articles.edit');
Route::delete('/articles/{article}/delete', [ArticleController::class, 'destroy'])->name('articles.destroy');
// Route::get('/articles/{article:title}/show', [ArticleController::class, 'show'])->name('articles.comment-show');

Route::get('/categories/{category}', [CategoryController::class, 'index'])->name('categories.index');
// Route::get('/tags/{tag}', [ArticleController::class, 'tagIndex'])->name('tags.index');

Route::get('/contact/index', [ContactController::class, 'index'])->name('contact.index');
Route::post('/contact/send', [ContactController::class, 'send'])->name('contact.send');
Route::get('/contact/thankyou', [ContactController::class, 'thankyou'])->name('contact.thankyou');

Route::get('#error', [ArticleController::class, 'store'])->name('footer.error');

Route::get('/search/index', [SearchController::class, 'index'])->name('search.index');

Route::middleware([AdminMiddleware::class])->group(function(){
    Route::get('/admin/dashboard', [AdminController::class, 'dashboard'])->name('admin.dashboard');
    Route::get('/articles/{user}', [AdminController::class, 'articlesForUser'])->name('articles.for.user');
    Route::delete('/users/{user}/delete', [AdminController::class, 'deleteUser'])->name('user.delete');
    Route::get('/toggle/{user}/disable', [AdminController::class, 'toggleUserDisable'])->name('toggle.user.disable');
    Route::delete('/admin/{article}/delete', [AdminController::class, 'destroy'])->name('articles.delete');
});

Route::post('/comment/{article}/store', [CommentController::class, 'store'])->name('comment.store')->middleware('auth');
// Route::get('/comment/show', [CommentController::class, 'show'])->name('comment.show');
Route::delete('/comment/{comment}/delete', [CommentController::class, 'destroy'])->name('comment.delete');