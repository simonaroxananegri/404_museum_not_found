<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Article;
use Illuminate\Http\Request;
use App\Mail\UserDeletedMail;
use App\Http\Requests\ErrorRequest;
use Illuminate\Support\Facades\Mail;


class AdminController extends Controller
{
    public function dashboard()
    {
        $users = User::orderBy('id','desc')->get();
        $articles = Article::orderBy('id','desc')->get();
        return view('admin.dashboard', compact('users','articles'));
    }

    public function articlesForUser(User $user)
    {
        $articles = $user->articles->sortDesc();
        return view('admin.user-articles', compact('articles','user'));
    }

    public function deleteUser(User $user, Request $request)
    {
        $message = $request->input('message');
        // Mail::to($user->mail)->send(new UserDeletedMail($message));
        $user->delete();
        return redirect()->back();
    }

    public function toggleUserDisable(User $user)
    {
        $user->disable = !$user->disable;
        $user->save();
        return redirect()->back();
    }

    public function destroy(Article $article)
    {
        $article->delete();
        
        return redirect(route('admin.dashboard'))->with('message', "L'articolo $article->title è stato cancellato");
    }

}
