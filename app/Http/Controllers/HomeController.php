<?php

namespace App\Http\Controllers;

use App\Models\Article;
use App\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

    public function home()
    {
        // $articles=Article::paginate(6); 
        // dd($article);
        $articles =Article::all();
        
        $articles->count();
        $articles=Article::orderBy('id','desc')->get();
        $lastsix = $articles->splice( 0,6); //prende gli ultimi 6 articoli

        // dd($lastsix);
         return view('home', compact('lastsix'));        

    }

    public function index()
    {
        $articles=Article::all();
        $categories = Category::all();
        
        return view('index', compact('articles', 'categories'));
    }
    

    public function profile()
    {
        $user=Auth::user();
        return view('users.profile', compact('user'));
    }


   
}
