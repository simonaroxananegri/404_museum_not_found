<?php

namespace App\Http\Controllers;

use App\Models\Tag;
use App\Models\Article;
use App\Models\Category;
use Illuminate\Http\Request;

class SearchController extends Controller
{
    public function index(Request $request){
        $search=$request->input('searchWord');
      
        $articles = Article::query()
         ->where('title', 'LIKE', "%{$search}%")
         ->orWhere('body', 'LIKE', "%{$search}%")
         ->get();

         $articles->tag = Tag::query()->where('name', 'LIKE', "%{$search}%" );
         $articles->category = Category::query()->where('name', 'LIKE', "%{$search}%" );

        

         

       


         return view('search.index', compact('articles'));
    }
}
