<?php

namespace App\Http\Controllers;

use App\Mail\ContactMail;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Mail;
use App\Http\Requests\ContactRequest;
use App\Http\Controllers\ContactController;

class ContactController extends Controller
{

    public function index(){
        return view('contact.index');
    }
    
    public function send(ContactRequest $request)
    {
        $name=$request->input('name');
        $email=$request->input('email');
        $message=$request->input('message');

        $content=compact('name', 'email', 'message');
        
        Mail::to('admin@miosito.it')->send(New ContactMail($content));

        return redirect( route('contact.thankyou'));
    }

    public function thankyou()
    {
        return view ('contact.thankyou');
    }
}
