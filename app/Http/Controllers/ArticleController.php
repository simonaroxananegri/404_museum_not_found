<?php

namespace App\Http\Controllers;

use App\Models\Tag;
use App\Models\Article;


use App\Models\Category;
use App\Http\Requests\ErrorRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use Symfony\Component\HttpFoundation\Request;



class ArticleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $articles=Article::orderBy('id', 'desc')->get();
        return view('articles.index', compact('articles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories=Category::all();
        
        if(Auth::user()->disable == 1){
            return redirect(route('home'))->with('message', "sei disabilitato");
        }

        return view('articles.create', compact('categories'));
        
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ErrorRequest $request)
    {
     
        
        $img = $request->file('img');
        
        if($img == null)
        {
            $article=Article::create([
                'title'=>$request->input('title'),
                'body'=>$request->input('body'),
                'img'=>$request->file('img'),
                'user_id'=>Auth::id(),
                'category_id'=>$request->input('category_id'),
                'solution'=>$request->input('solution'),
                'tag_id'=>$request->input('tag_id')
               
                
            ]);

            $tags = collect($request->input('tag'));
            $tags->each(function($tag) use($article){
                $article->tags()->attach($tag);
                });
            
                
            
                return redirect(route('home'))->with('message', "L'articolo $article->title è stato creato con successo"); 
               
        }else{     
        
            $article=Article::create([
                'title'=>$request->input('title'),
                'body'=>$request->input('body'),
                'img'=>$request->file('img')->store('public'),
                'user_id'=>Auth::id(),
                'category_id'=>$request->input('category_id'),
                'solution'=>$request->input('solution'),
                'tag_id'=>$request->input('tag_id')
            ]);

            $tags = collect($request->input('tag'));
            $tags->each(function($tag) use($article){
                $article->tags()->attach($tag);
                });
          
            return redirect(route('home'))->with('message', "L'articolo $article->title è stato creato con successo"); 
        }
    }
      
    
     
    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Article  $article
     * @return \Illuminate\Http\Response
     */
    
  
    
    public function show($id){
        
        $article=Article::find($id);
        return view('articles.show', compact('article'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function edit(Article $article)
    {
        $categories=Category::all();
        return view('articles.edit', compact('categories', 'article'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Article $article)
    {
        $article->update($request->all());
        return redirect(route('users.profile'))->with('message', "L' articolo $article->title è stato modificato correttamente.");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function destroy(Article $article)
    {
        $article->delete();
        return redirect()->back()->with('message', "L' articolo $article->title è stato cancellato con successo.");
    }

    public function tagIndex(Tag $tags)
    {
        return view('tags.index', compact('tags'));
    }
}
