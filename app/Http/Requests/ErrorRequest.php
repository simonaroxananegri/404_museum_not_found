<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ErrorRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
           'title'=> 'required|min:3|max:20',
           'body' => 'required',
           'img' => 'nullable',
           'solution' => 'nullable'
        ];
    }
    public function messages(){
        return [
                
                'body.required'=>"La descrizione è obbligatoria",
                'title.required'=>"Il titolo è obbligatorio",
                'title.min'=>"Il titolo deve essere di minimo 3 caratteri",
                'title.max'=>"Il titolo deve essere di massimo 20 caratteri"
                ];

                
           
            
    }
}
